# Diogo Campos

Este repositório, por enquanto, contém:

- A página pessoal de [Diogo Campos](https://o-diogocampos.rhcloud.com/).
- A associação de profissionais autônomos [Autonomia](https://o-diogocampos.rhcloud.com/autonomia.html).
- A história interativa [Aline Em Lugar Algum](https://o-diogocampos.rhcloud.com/aline-em-lugar-algum.html).
- A sátira de futebol e mídia [Cubo Esporte](https://o-diogocampos.rhcloud.com/cubo-esporte.html).
- A sátira de mídia de massa e comportamento [C4](https://o-diogocampos.rhcloud.com/c4.html).
- A obra literária sem sentido [A Solução Para Tudo](https://o-diogocampos.rhcloud.com/a-solucao-para-tudo.html).

# Licenças

Todo o código-fonte desse repositório está licenciado sob a [GNU AGPL v3](https://www.gnu.org/licenses/agpl.html).

Todos os outros arquivos estão sob a [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/).

# Créditos

## Página pessoal

[Palomaironique](https://openclipart.org/user-detail/palomaironique), por "[Butterfly 02 Turquoise Blue](https://openclipart.org/detail/57481/butterfly-02-turquoise-blue)", sob a licença [CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/).

## Aline Em Lugar Algum

[HeyYO](https://openclipart.org/user-detail/HeyYO), por "[Eyeglasses](https://openclipart.org/detail/173095/eyeglasses)", sob a licença [CC Public Domain](http://creativecommons.org/licenses/publicdomain/).

[Cyreal](http://www.cyreal.org), por "[Alice](https://www.google.com/fonts/specimen/Alice)", sob a licença [SIL OFL 1.1](http://scripts.sil.org/OFL).

## A Solução Para Tudo

[Vernon Adams](mailto:vern@newtypography.co.uk), por "[Anton](https://www.google.com/fonts/specimen/Anton)", sob a licença [SIL OFL 1.1](http://scripts.sil.org/OFL).

## Cubo Esporte

[Matt McInerney](mailto:matt@pixelspread.com), por "[Orbitron](https://www.google.com/fonts/specimen/Orbitron)", sob a licença [SIL OFL 1.1](http://scripts.sil.org/OFL).

[inky2010](https://openclipart.org/user-detail/inky2010), por "[Glossy shields 1](https://openclipart.org/detail/78499/glossy-shields-1)", sob a licença [CC0 1.0](http://creativecommons.org/publicdomain/zero/1.0/).

[netalloy](https://openclipart.org/user-detail/netalloy), por "[Trophy](https://openclipart.org/detail/120343/trophy)", sob a licença [CC0 1.0](http://creativecommons.org/publicdomain/zero/1.0/).

### América Amazonense

[Juan Pablo del Peral](mailto:juan@huertatipografica.com.ar), por "[Alegreya Sans](https://www.google.com/fonts/specimen/Alegreya+Sans)", sob a licença [SIL OFL 1.1](http://scripts.sil.org/OFL).

### Atlético Sul-Mato-Grossense

[Natanael Gama](mailto:info@ndiscovered.com), por "[Cinzel](https://www.google.com/fonts/specimen/Cinzel)", sob a licença [SIL OFL 1.1](http://scripts.sil.org/OFL).

[Gribba](https://openclipart.org/user-detail/Gribba), por "[Classic medieval sword](https://openclipart.org/detail/169233/classic-medieval-sword)", sob a licença [CC0 1.0](http://creativecommons.org/publicdomain/zero/1.0/).

### Biguaçú

[Soytutype](mailto:contact@soytutype.com.ar), por "[Oleo Script](https://www.google.com/fonts/specimen/Oleo+Script)", sob a licença [SIL OFL 1.1](http://scripts.sil.org/OFL).

[Giro720](https://commons.wikimedia.org/wiki/User:Giro720), por "[Brasao Biguacu SantaCatarina Brasil](https://commons.wikimedia.org/wiki/File:Brasao_Biguacu_SantaCatarina_Brasil.svg?uselang=pt)", sob a licença [Domínio público](https://pt.wikipedia.org/wiki/Domínio_público).

### CRJ

[Matt McInerney](mailto:matt@pixelspread.com), por "[Orbitron](https://www.google.com/fonts/specimen/Orbitron)", sob a licença [SIL OFL 1.1](http://scripts.sil.org/OFL).

### Curvelana

[Jonas Hecksher, Playtypes, e-types AS](e-types.com), por "[Play](https://www.google.com/fonts/specimen/Play)", sob a licença [SIL OFL 1.1](http://scripts.sil.org/OFL).

[Sev](https://openclipart.org/user-detail/Sev), por "[Soccer ball](https://openclipart.org/detail/222957/soccer-ball)", sob a licença [CC0 1.0](http://creativecommons.org/publicdomain/zero/1.0/).

### Desbravante

[Indian Type Foundry](mailto:info@indiantypefoundry.com), por "[Halant](https://www.google.com/fonts/specimen/Halant)", sob a licença [SIL OFL 1.1](http://scripts.sil.org/OFL).

[mr_johnnyp](https://openclipart.org/user-detail/mr_johnnyp), por "[Jubilee crown red](https://openclipart.org/detail/170035/jubilee-crown-red)", sob a licença [CC0 1.0](http://creativecommons.org/publicdomain/zero/1.0/).

[GDJ](https://openclipart.org/user-detail/GDJ), por "[Sailboat Silhouette](https://openclipart.org/detail/223020/sailboat-silhouette)", sob a licença [CC0 1.0](http://creativecommons.org/publicdomain/zero/1.0/).

### Inter de Piraquara

[LatinoType](mailto:luciano@latinotype.com), por "[Sanchez](https://www.google.com/fonts/specimen/Sanchez)", sob a licença [SIL OFL 1.1](http://scripts.sil.org/OFL).

### Piauí

[Brian J. Bonislawsky DBA Astigmatic (AOETI)](mailto:astigma@astigmatic.com), por "[Righteous](https://www.google.com/fonts/specimen/Righteous)", sob a licença [SIL OFL 1.1](http://scripts.sil.org/OFL).

[Pentelhojin](https://commons.wikimedia.org/wiki/User:Pentelhojin), por "[Bandeiradepicos](https://commons.wikimedia.org/wiki/File:Bandeiradepicos.svg?uselang=pt)", sob a licença [Domínio público](https://pt.wikipedia.org/wiki/Domínio_público).

[TUBS](https://commons.wikimedia.org/wiki/User:TUBS), por "[Piaui in Brazil](https://commons.wikimedia.org/wiki/File:Piaui_in_Brazil.svg?uselang=pt)", sob a licença [CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/).

### Pinheirense

[Admix Designs](http://www.admixdesigns.com/), por "[Questrial](https://www.google.com/fonts/specimen/Questrial)", sob a licença [SIL OFL 1.1](http://scripts.sil.org/OFL).

### Santa Marta

[TypeSETit, LLC](mailto:typesetit@att.net), por "[Italianno](https://www.google.com/fonts/specimen/Italianno)", sob a licença [SIL OFL 1.1](http://scripts.sil.org/OFL).
## Capítulo 1: Apresenta Ela

E o mundo continua o mesmo. O sol continua surgindo, as flores continuam crescendo, e as pessoas continuam se odiando. E ela continua atrasada. Como sempre. Aliás, ali vai ela, numa mistura graciosa de andar e correr. Quase corre para sentir-se responsável, não pôr mais fogo no inferno interno. Quase anda para ver se consegue, talvez, nunca chegar. Ou, ao menos, adiar o máximo possível. Algo que ela sempre consegue; e com extrema excelência. Algo que sempre culpa-se por.

Atendente em um ilustre comércio de *Shit-Food* situado no incrível *Money-Planet*. Não é algo que goste. Mesmo! E muito menos algo que se orgulha. Claro! Mas soa, para ela, ligeiramente melhor do que a eternidade que passou desempregada. Ou não. Na verdade, não quer *mesmo* pensar nisso, só quer chegar ao seu destino. E, depois, ao próximo: maratona de 4 horas de sermão com os Mestres do Universo, cercada de mais de 20 neo-machistas de menos de 20 anos criados a creme de avelã com cacau. "Facu", para os íntimos. O nome do curso ela nem lembra. Só lhe importam os algoritmos.

E depois? Voltar para o lar-não-tão-doce-lar, e morrer por 6 ou 8 horas. Morrer forte. Morrer pesado. Morrer para sempre. Para ela, a melhor parte do dia é a noite. Pois dormir carrega uma semelhança muito alta à morte para ela não o ter como sua atividade preferida. As noites sem sonhos, as melhores; pacíficas. Pois ela sonha, mesmo, é acordada. Como neste exato momento, inclusive. Destravou a visão do horizonte; olhou o horário; não o entendeu; mas acelerou mesmo assim. Pois estar atrasada, como visto, já sabia. E sabia também s...

O CARTÃO! - pensou de repente.

MERDA! - gritou.

Deu meia-volta. Enfurecida.

Um óculos preto, de grau, simples. Que auxilia e proteje um lindo par de olhos castanhos, já sem brilho. Um óculos sustentado por sutis orelhas com fones de ouvido integrados, e por um belo nariz sofrido. Um óculos que combina perfeitamente com o sorriso que ela acabou de dar, ao ver um gatinho andando pela rua, enquanto agora corre. Entretanto, sejamos justos, o que é que não combinaria com um sorriso desses? Porquê essa garota insiste em enscondê-lo por trás desses lábios roxo-hipnose? O movimento seguinte, já mecânico e automático, porém ainda suave e elegante, de tirar o cabelo dos olhos enquanto toca a têmpora de leve, nos faz chegar ao quinto sentido: essa pele branco-miscigenada já sentiu a auto-agressão, mas persiste produzindo carinho.

Cabelo preto; natural; curto. Espetacular. Um rosto redondo, com bochechas proeminentes, averso à maquiagens. 165 centímetros do mais puro e carismático desengonço. Peso? Bem, ela diz para si mesma, internamente, e a cada 10 minutos, que precisa perder uns 10 quilos para normalizar o seu externo. Poderia dizer que, talvez, ela tenha razão, mas, porquê eu cometeria tal atrocidade? Prefiro admirá-la, e por completo, incluindo o vestuário do dia: uma camiseta básica roxa na altura da cintura, uma calça justa preta até a canela, e um par de sapatinhos roxos, com lacinho, bem abaixo do tornozelo. Nos braços, cadernos; nos ombros, uma pequena bolsa preta; e, nas unhas, você já deve imaginar qual cor. Veredito final: assustadoramente encantadora. É apenas uma imensa pena que ela não concorde com nada disso. Absolutamente nada.

Finalmente chegou. Embora ao destino oposto. E com os dedinhos ela abriu a porta de casa, pegou o cartão-crachá-ponto, arrumou os cabelos na frente do espelho, e fechou a porta de casa. Deu dois passos à frente, hesitou, deu dois passos para trás, conferiu se tinha mesmo trancado a porta, e seguiu para o destino original. Finalmente.

Mas é claro que ela é vaidosa! Uma generosa dose de estrogênio circula por esse corpo, como haveria de ser diferente? Aliás, ela também é muitas outras coisas. Uma mais positiva, bem intencionada e doce do que a outra, acredite. Tantas, e tão extasiantes, que fica até difícil listar. Entretanto, ao contrário dela, ainda temos bastante tempo, então, por onde começar? Que tal uma visão geral? Quem vê essa garota correndo por aí (como agora); de cara triste, apática ou brava (como agora); sempre com fones de ouvido, aparentemente alheia ao mundo (como agora); não faz idéia do quanto ela tenta perceber, e melhorar, o mundo a sua volta. O fino e frágil escudo negativo-mórbido é só algo que ela usa (ingenuamente) para tentar se proteger de algo, ainda abstrato, que insiste em tornar tudo tão mais difí...

BLAM! - o tempo congela.

Aline Borges, 25 anos, não viu o maldito carro vermelho quando atravessou a rua correndo. Muito menos o ouviu. Tudo que ouviu, foi: "você é covarde demais!", nos fones de ouvido, antes de bater violentamente com a têmpora no capô do carro.

[...] - o tempo avança, mas vagarosamente.

É possível ver os fones de ouvido sendo arremessados, logo após o impacto, junto com o óculos. Ela própria é arremessada. Tudo flutua, só que com requintes de crueldade. O delicado corpo dela vai se dobrando em posição antes impossível. Da cabeça, um amassado vaza fluído *O positivo*. Dos olhos, já não há movimento ou foco. Do braço, ossos acessam a luz do dia pela primeira vez, às custas da própria carne. Das mãos, fogem os livros e, talvez, também os sonhos.

[...] - o tempo volta ao normal.

E, do ar, em queda-livre, Aline cai como uma boneca no chão. Imóvel. Sangrando. Torta. Horrível.

[...] - o tempo se recusa a recuar.

O motorista algoz sai do carro, ligeiramente transtornado, mas inteiramente confiante de estar protegido pela lei máxima de que "a rua é dos carros". Aline no chão; culpada.

A testemunha ocular, em choque, leva uma das mãos a boca, e evoca um deus qualquer. Aline no chão; abençoada.

A multidão multiplica-se; sob a premissa de que um show de horror é muito mais interessante do que um cotidiano, a quantidade de olhos na horizontal e de câmeras na vertical aumenta. Aline no chão; admirada.

Alguém se aproxima do corpo rapidamente, abaixa-se, toca no pescoço, e afirma ao celular: "ela está respirando".

Aline arregala os olhos repentinamente. Assustada. Esbaforida. Não reconhece nada. As memórias não estão acessíveis. Amedronta-se. Resolve levantar rápido e olhar ao redor. Até consegue se levantar, mas tudo dói, e muito; especialmente a cabeça e o braço. Tudo o que consegue notar, do ambiente que a cerca, é nada. Muito escuro. Ela pressiona o óculos contra o rosto, com o intuito de enxergar melhor, e imediatamente cruza olhares com... Aquilo (seja lá o que "Aquilo" for). Aline trava.

Ainda com o olhar fixo, e o dedo no óculos, ela decide dar um passo atrás. Aquilo imediatamente deu um passo à frente, talvez em reflexo, quase que em sincronia. Se havia espaço para ficar ainda mais assustada, posso garantir que ficou. E, por isso mesmo, deu mais um passo atrás, dessa vez lentamente. Não ficou barato; Aquilo também deu mais um passo lento, à frente. Aline re-travou. Não sabia o que fazer. Em contrapartida, Aquilo pareceu saber bem, e começou a correr em direção a ela.

AAA! - gritou ela, de súbito.

E imediatamente deu meia-volta, jogou os braços ao ar e se pôs a correr. Vulgo fugir. Com o corpo dolorido, o ritmo não era dos melhores. Entretanto, Aquilo, por qualquer razão, não parecia ter vantagem; também mal conseguia correr.

Permita-me pular a correria frenética, por duas razões: primeiro, porque "frenéticos" eram somente os batimentos cardíacos; a corrida, naquele estado, era de dar dó. E, segundo, porque não durou mais do que 20 segundos, pois, em dado momento, Aline não sentiu o chão aos pés e... perdoe-me, devo dizer, o que se sucedeu foi tão cômico quanto trágico: tanto Aline quanto Aquilo se perceberam, de repente, rolando colina abaixo da forma mais grotesco-graciosa possível. Entendo que essa parte deveria ter sido repleta de ação, intensidade e explosões; mas não consigo mentir para você, esses últimos 25 segundos foram mesmo é sutilmente engraçados.

Tanto é que quando Aline, com dificuldade, se levantou, até arriscou um sorriso, mas foi sumariamente interrompida por um barulho intenso, estranho, intimidador e ensurdecedor. Virou a cabeça, instantaneamente, para a origem do som e voltou a cruzar olhares com Aquilo. Seus olhos, involuntariamente, esbugalharam-se. Seu corpo começou a tremer descontroladamente. E cada uma de suas vértebras sentiu a gélida certeza de que o tal barulho havia sido o anúncio do "segundo *round*"; e de que agora era para valer, vida ou morte, ápice dos clichês; e de que, depois disso, talvez o mundo não continue o mesmo.

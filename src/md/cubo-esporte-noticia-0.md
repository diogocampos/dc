# BOMBA! Biguaçú contrata fortíssimo reforço para a próxima temporada.

É oficial! Após uma transação deveras milionária o Biguaçu Sport Club agora é o mais novo contratante do craque Dieguinho Itaquaquecetubense, a estrela maior do futebol global.

Dieguinho, que se destacou nas divisões de base nacionais e logo foi vendido para o cultuado futebol do Azerbaijão, agora volta com toda sua glória para seu país natal para defender com unhas e dentes a sólida tradição do Biguaçú.

![Dieguinho](img/ce-n1-i1.jpg)

*"Estou bem melhor e vou dar meu melhor"*, disse o atacante, em entrevista exclusiva para o Cubo Esporte. Afastado dos gramados à 3 anos, por conta de uma contusão no tornozelo direito, Dieguinho vem galgando uma recuperação sólida e heróica.

Nas ruas, a resposta da torcida é extremamente positiva, como visto no semblante de Breno de Assis, de 33 anos, que afirmou *"Melhor coisa que aconteceu na minha vida nos últimos anos"*. Mas também há alguns torcedores pessimistas: _"Mercenário alejado filho da p\*\*\*"_, disse Danilo Farias, de 14; e *"Quê?"* destacou Seu Juvenal, no alto de seus 78 anos de experiência.

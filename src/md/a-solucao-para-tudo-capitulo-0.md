## Introdução

Ao longo de eras perguntamo-nos o que há de haver além da conspiração. Pois bem, não mais. Se há algo que venho testemunhado ao longo desses 25 anos de luxúria cognitiva, com certeza é a incapacidade humana de conter vazamentos adstringentes de ordem moral. Algum momento havia de ser. Mas por quanto? Se o agora se faz presente no hoje, que embarquemos nessa virtude constante.

“Tão interior”, eles diriam. Que seja. A necessidade de ação se faz estonteante pois não haveria de ser outrora. É um formigamento sucinto, porém sim. E deste, sempre mirando à diagonal, soberano aos erros não vis, se faz a escolha do destino sucinto apaziguado. Daqui? Pra sempre? Saberemos, apenas, ao prosseguir.

E cá estamos, não há como confirmar, somente é. Testemunhemos e ajamos. Porquê o giro orbital, por si, nada moverá.

Por fim, o começo: esta obra, salvo suas deficiências, tem por intuito interagir com a situação calamitosa psicológica presente e itinerante na gama intelectual da sociedade contemporânea; tem, como dito, um intuito; mas falta fagulha. Poderá ser a fagulha sua instância, caro leitor, caso haja sincera protuberância advertida de seu espectro. Ou todo o proposto cairá por terras irrigadas. Se me permite almejar, sinceramente, aguardo sua compreensão.

Ajamos e testemunhemos, portanto.

Separamo-nos no infinito. Encontraremo-nos no fim.

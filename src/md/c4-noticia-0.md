# Banda Osteoartrose lançará seu primeiro EP

A banda gótica Osteoartrose, sucesso absoluto entre os jovens de classe média-alta por conta do modo de vestir e da atitude de seus integrantes, resolveu inovar e lancará seu primeiro trabalho musical em breve.

![Cartaz](img/c4-n1-i1.jpg)

O EP, que no momento está em processo de pré-produção, se chamará *Computerized Tomography* e contará com prováveis *hits* como *Bat Bath*, *Slaughter*, *Red Lipstick of Death* e *Grape Babalu*.

O local de lançamento não poderia ser outro senão o conceituadíssimo *Catacomb Club*. Amplo estacionamento, *Open Bar* e rituais satânicos com total ausência de nudez fazem do local uma excelente opção para curtir uma bela tarde com seus filhos.

/* jshint browser: true */

var cde = {};

function atualizarHTMLContribuicao() {
    "use strict";
    if (cde.opcaoReais.checked === true) {
        /* Como é o suporte e padronização do ".parentNode"? */
        cde.opcaoReais.parentNode.className = "selecionado";
        cde.opcaoBitcoins.parentNode.className = "";
        cde.blocoReais.style.display = "block";
        cde.blocoBitcoins.style.display = "none";
    } else if (cde.opcaoBitcoins.checked === true) {
        cde.opcaoReais.parentNode.className = "";
        cde.opcaoBitcoins.parentNode.className = "selecionado";
        cde.blocoReais.style.display = "none";
        cde.blocoBitcoins.style.display = "block";
    }
}

function iniciar() {
    "use strict";
    if (dcl === true) {
        cde.opcaoReais = document.querySelector("#opcao-reais");
        cde.opcaoBitcoins = document.querySelector("#opcao-bitcoins");
        cde.blocoReais = document.querySelector("#bloco-reais");
        cde.blocoBitcoins = document.querySelector("#bloco-bitcoins");
        cde.opcaoReais.addEventListener("click", atualizarHTMLContribuicao, false);
        cde.opcaoBitcoins.addEventListener("click", atualizarHTMLContribuicao, false);
        atualizarHTMLContribuicao();
    } else {
        window.setTimeout(iniciar, 100);
    }
}

iniciar();

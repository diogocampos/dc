/* jshint browser: true */

function start() {
    "use strict";
    var app = document.querySelector("#app");
    
    /* VARIÁVEIS */
    app.pagina = null;
    app.esconderBotoesPrincipais = false;
    app.esconderBotoesSecundarios = true;
    app.esconderSpinner = true;
    app.urlNoticiaPrefixo = "md/c4-noticia-";
    app.urlNoticiaSufixo = ".md";
    app.urlNoticia = null;
    app.noticia = null;
    app.hash = null;
    app.noticias = [
        {
            "titulo": "Banda Osteoartrose lançará seu primeiro EP."
        }
    ];
    
    /* FUNÇÕES */
    app.analisarHash = function () {
        if (app.hash === "noticias") {
            app.pagina = 1;
        } else if (app.hash === "sobre") {
            app.pagina = 2;
        } else {
            app.pagina = 1;
        }
    };
    app.processarNoticia = function (event) {
        var r = event.detail.response;
        app.esconderSpinner = true;
        app.noticia = r;
    };
    app.mostrarNoticia = function (e) {
        var numero = e.target.getAttribute("data-noticia-numero");
        app.noticia = null;
        app.esconderSpinner = false;
        app.urlNoticia = null;
        app.urlNoticia = app.urlNoticiaPrefixo + numero + app.urlNoticiaSufixo;
        app.esconderBotoesPrincipais = true;
        app.esconderBotoesSecundarios = false;
        app.pagina = 0;
    };
    app.esconderNoticia = function () {
        app.esconderBotoesPrincipais = false;
        app.esconderBotoesSecundarios = true;
        app.pagina = 1;
    };
    app.alterarPagina = function (e) {
        app.hash = e.target.getAttribute("data-pagina-hash");
        app.analisarHash();
    };
    
    /* INÍCIO */
    app.analisarHash();
}

document.addEventListener("WebComponentsReady", function() {
    "use strict";
    start();
});
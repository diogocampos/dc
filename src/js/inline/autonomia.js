/* jshint browser: true */

function start() {
    "use strict";
    var app = document.querySelector("#app");
    var c = document.querySelector("#mainContainer");
    /* Variáveis */
    app.pagina = 0;
    app.desabilitarBotaoPrincipal = true;
    app.desabilitarBotaoSistemas = false;
    app.desabilitarBotaoFaq = false;
    /* Funções */
    app.irPaginaPrincipal = function () {
        c.scrollTop = 0;
        app.pagina = 0;
        app.desabilitarBotaoPrincipal = true;
        app.desabilitarBotaoSistemas = false;
        app.desabilitarBotaoFaq = false;
    };
    app.irPaginaSistemas = function () {
        c.scrollTop = 0;
        app.pagina = 1;
        app.desabilitarBotaoPrincipal = false;
        app.desabilitarBotaoSistemas = true;
        app.desabilitarBotaoFaq = false;
    };
    app.irPaginaFaq = function () {
        c.scrollTop = 0;
        app.pagina = 2;
        app.desabilitarBotaoPrincipal = false;
        app.desabilitarBotaoSistemas = false;
        app.desabilitarBotaoFaq = true;
    };
}

document.addEventListener("WebComponentsReady", function() {
    "use strict";
    start();
});

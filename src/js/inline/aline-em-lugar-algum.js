/* jshint browser: true */

function start() {
    "use strict";
    var app = document.querySelector("#app");
    
    /* VARIÁVEIS */
    app.pagina = null;
    app.esconderBotoesPrincipais = false;
    app.esconderBotoesSecundarios = true;
    app.esconderSpinner = true;
    app.urlCapituloPrefixo = "md/aline-em-lugar-algum-capitulo-";
    app.urlCapituloSufixo = ".md";
    app.urlCapitulo = null;
    app.capitulo = null;
    app.hash = null;
    app.capitulos = [
        {
            "titulo": "Capítulo 1: Apresenta Ela"
        },
        {
            "titulo": "Capítulo 2: ???"
        }
    ];
    
    /* FUNÇÕES */
    app.analisarHash = function () {
        if (app.hash === "capitulos") {
            app.pagina = 1;
        } else if (app.hash === "sobre") {
            app.pagina = 2;
        } else {
            app.pagina = 1;
        }
    };
    app.processarCapitulo = function (event) {
        var r = event.detail.response;
        app.esconderSpinner = true;
        app.capitulo = r;
    };
    app.mostrarCapitulo = function (e) {
        var numero = e.target.getAttribute("data-capitulo-numero");
        app.capitulo = null;
        app.esconderSpinner = false;
        app.urlCapitulo = null;
        app.urlCapitulo = app.urlCapituloPrefixo + numero + app.urlCapituloSufixo;
        app.esconderBotoesPrincipais = true;
        app.esconderBotoesSecundarios = false;
        app.pagina = 0;
    };
    app.esconderCapitulo = function () {
        app.esconderBotoesPrincipais = false;
        app.esconderBotoesSecundarios = true;
        app.pagina = 1;
    };
    app.alterarPagina = function (e) {
        app.hash = e.target.getAttribute("data-pagina-hash");
        app.analisarHash();
    };
    
    /* INÍCIO */
    app.analisarHash();
}

document.addEventListener("WebComponentsReady", function() {
    "use strict";
    start();
});
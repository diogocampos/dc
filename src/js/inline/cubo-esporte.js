/* jshint browser: true */

function start() {
    "use strict";
    var app = document.querySelector("#app");
    
    /* VARIÁVEIS */
    app.pagina = null;
    app.esconderBotoesPrincipais = false;
    app.esconderBotoesSecundarios = true;
    app.esconderSpinner = true;
    app.urlNoticiaPrefixo = "md/cubo-esporte-noticia-";
    app.urlNoticiaSufixo = ".md";
    app.urlNoticia = null;
    app.noticia = null;
    app.hash = null;
    app.times = [
        {
            "nome": "América Amazonense",
            "nomeCompleto": "América Amazonense Esporte Clube",
            "cidadeNome": "Tabatinga",
            "cidadeLink": "https://pt.wikipedia.org/wiki/Tabatinga_(Amazonas)",
            "estadoNome": "Amazonas",
            "estadoLink": "https://pt.wikipedia.org/wiki/Amazonas",
            "caminho": "america"
        },
        {
            "nome": "Atlético Sul-Mato-Grossense",
            "nomeCompleto": "Clube Atlético Sul-Mato-Grossense",
            "cidadeNome": "Jardim",
            "cidadeLink": "https://pt.wikipedia.org/wiki/Jardim_(Mato_Grosso_do_Sul)",
            "estadoNome": "Mato Grosso do Sul",
            "estadoLink": "https://pt.wikipedia.org/wiki/Mato_Grosso_do_Sul",
            "caminho": "atletico"
        },
        {
            "nome": "Biguaçú",
            "nomeCompleto": "Biguaçú Sport Club",
            "cidadeNome": "Biguaçú",
            "cidadeLink": "https://pt.wikipedia.org/wiki/Biguaçu",
            "estadoNome": "Santa Catarina",
            "estadoLink": "https://pt.wikipedia.org/wiki/Santa_Catarina",
            "caminho": "biguacu"
        },
        {
            "nome": "CRJ",
            "nomeCompleto": "Clube de Regatas de Juína",
            "cidadeNome": "Juína",
            "cidadeLink": "https://pt.wikipedia.org/wiki/Juína",
            "estadoNome": "Mato Grosso",
            "estadoLink": "https://pt.wikipedia.org/wiki/Mato_Grosso",
            "caminho": "crj"
        },
        {
            "nome": "Curvelana",
            "nomeCompleto": "Associação Curvelana de Futebol",
            "cidadeNome": "Curvelo",
            "cidadeLink": "https://pt.wikipedia.org/wiki/Curvelo",
            "estadoNome": "Minas Gerais",
            "estadoLink": "https://pt.wikipedia.org/wiki/Minas_Gerais",
            "caminho": "curvelana"
        },
        {
            "nome": "Desbravante",
            "nomeCompleto": "Desbravante Futebol Clube",
            "cidadeNome": "Buritis",
            "cidadeLink": "https://pt.wikipedia.org/wiki/Buritis_(Rondônia)",
            "estadoNome": "Rondônia",
            "estadoLink": "https://pt.wikipedia.org/wiki/Rondônia",
            "caminho": "desbravante"
        },
        {
            "nome": "Inter de Piraquara",
            "nomeCompleto": "Sociedade Esportiva Internacional de Piraquara",
            "cidadeNome": "Piraquara",
            "cidadeLink": "https://pt.wikipedia.org/wiki/Piraquara",
            "estadoNome": "Paraná",
            "estadoLink": "https://pt.wikipedia.org/wiki/Paraná",
            "caminho": "inter"
        },
        {
            "nome": "Piauí",
            "nomeCompleto": "Clube Piauí de Desporto",
            "cidadeNome": "Picos",
            "cidadeLink": "https://pt.wikipedia.org/wiki/Picos",
            "estadoNome": "Piauí",
            "estadoLink": "https://pt.wikipedia.org/wiki/Piauí",
            "caminho": "piaui"
        },
        {
            "nome": "Pinheirense",
            "nomeCompleto": "Esporte Clube Pinheirense",
            "cidadeNome": "Pinheiro",
            "cidadeLink": "https://pt.wikipedia.org/wiki/Pinheiro_(Maranhão)",
            "estadoNome": "Maranhão",
            "estadoLink": "https://pt.wikipedia.org/wiki/Maranhão",
            "caminho": "pinheirense"
        },
        {
            "nome": "Santa Marta",
            "nomeCompleto": "Santa Marta Football Club",
            "cidadeNome": "Viana",
            "cidadeLink": "https://pt.wikipedia.org/wiki/Viana_(Espírito_Santo)",
            "estadoNome": "Espírito Santo",
            "estadoLink": "https://pt.wikipedia.org/wiki/Espírito_Santo_(estado)",
            "caminho": "santa-marta"
        }
    ];
    app.noticias = [
        {
            "titulo": "BOMBA! Biguaçú contrata fortíssimo reforço para a próxima temporada."
        }
    ];
    
    /* FUNÇÕES */
    app.analisarHash = function () {
        if (app.hash === "noticias") {
            app.pagina = 1;
        } else if (app.hash === "times") {
            app.pagina = 2;
        } else if (app.hash === "campeonato") {
            app.pagina = 3;
        } else if (app.hash === "sobre") {
            app.pagina = 4;
        } else {
            app.pagina = 1;
        }
    };
    app.processarNoticia = function (event) {
        var r = event.detail.response;
        app.esconderSpinner = true;
        app.noticia = r;
    };
    app.mostrarNoticia = function (e) {
        var numero = e.target.getAttribute("data-noticia-numero");
        app.noticia = null;
        app.esconderSpinner = false;
        app.urlNoticia = null;
        app.urlNoticia = app.urlNoticiaPrefixo + numero + app.urlNoticiaSufixo;
        app.esconderBotoesPrincipais = true;
        app.esconderBotoesSecundarios = false;
        app.pagina = 0;
    };
    app.esconderNoticia = function () {
        app.esconderBotoesPrincipais = false;
        app.esconderBotoesSecundarios = true;
        app.pagina = 1;
    };
    app.alterarPagina = function (e) {
        app.hash = e.target.getAttribute("data-pagina-hash");
        app.analisarHash();
    };
    
    /* INÍCIO */
    app.times.forEach(function(cv, i) {
        var prefixo = "img/", sufixo = ".svg";
        app.times[i].caminhoEscudo = prefixo + "escudo-" + app.times[i].caminho + sufixo;
        app.times[i].caminhoUniforme = prefixo + "uniforme-" + app.times[i].caminho + sufixo;
    });
    app.analisarHash();
}

document.addEventListener("WebComponentsReady", function() {
    "use strict";
    start();
});
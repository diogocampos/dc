/* jshint node: true */

var gulp = require('gulp');
var inline = require('gulp-inline-source');
var minifyJS = require('gulp-uglify');
var minifyIMG = require('gulp-imagemin');
var lintJS = require('gulp-jshint');
var checkJS = require('gulp-jscs');
var vulcanize = require('gulp-vulcanize');

gulp.task('lint-js', function () {
    'use strict';
    return gulp.src(['*.js', 'src/js/*.js', 'src/js/inline/*.js'])
        .pipe(lintJS())
        .pipe(lintJS.reporter())
        .pipe(checkJS())
        .pipe(checkJS.reporter());
});

gulp.task('lint', ['lint-js'], function () {
});

gulp.task('copy-bower-2', function () {
    'use strict';
    return gulp.src(['bower_components/web-animations-js/web-animations-next-lite.min.js'])
        .pipe(gulp.dest('dist/bower_components/web-animations-js/'));
});

gulp.task('copy-bower', function () {
    'use strict';
    return gulp.src(['bower_components/webcomponentsjs/webcomponents-lite.min.js'])
        .pipe(gulp.dest('dist/bower_components/webcomponentsjs/'));
});

gulp.task('copy-stuff', function () {
    'use strict';
    return gulp.src('src/stuff/*')
        .pipe(gulp.dest('dist/'));
});

gulp.task('copy-all', ['copy-stuff', 'copy-bower', 'copy-bower-2'], function () {
});

gulp.task('build-img', function () {
    'use strict';
    return gulp.src('src/img/*.*')
        .pipe(minifyIMG())
        .pipe(gulp.dest('dist/img/'));
});

gulp.task('copy-js', function () {
    'use strict';
    return gulp.src('src/js/libs/*.js')
        .pipe(gulp.dest('dist/js/libs/'));
});

gulp.task('build-js', ['copy-js'], function () {
    'use strict';
    return gulp.src('src/js/*.js')
        .pipe(minifyJS())
        .pipe(gulp.dest('dist/js/'));
});

gulp.task('build-css', function () {
});

gulp.task('build-iconsets', function () {
    'use strict';
    return gulp.src('src/img/iconsets/*.html')
        .pipe(inline({compress: false}))
        .pipe(gulp.dest('bower_components/iron-icons/'));
});

gulp.task('build-elements', ['build-iconsets'], function () {
    'use strict';
    return gulp.src(['src/index-elementos.html', 'src/1995-elementos.html'])
        .pipe(vulcanize())
        .pipe(gulp.dest('dist/'));
});

gulp.task('build-html', ['build-elements'], function () {
    'use strict';
    return gulp.src(['src/index.html', 'src/1995.html'])
        .pipe(gulp.dest('dist/'));
});

gulp.task('default', ['build-html', 'build-css', 'build-js', 'build-img', 'copy-all']);
